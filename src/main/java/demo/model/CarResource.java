package demo.model;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Collection;


@Path("/car")
@ApplicationScoped
public class CarResource {

    @PersistenceContext(unitName = "AdminPU")
    private EntityManager em;

    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Car> all() throws Exception {
        return em.createNamedQuery("Car.findAll", Car.class)
                .getResultList();
    }
}